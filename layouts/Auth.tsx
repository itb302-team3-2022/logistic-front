import Image from 'next/image'

// components

import { Navbar, FooterSmall } from 'components'

type Props = {
  children: React.ReactNode
}

const Auth: React.FC<Props> = ({ children }) => {
  return (
    <>
      <Navbar transparent />
      <main>
        <section className="relative w-full h-full py-40 min-h-screen">
          <Image
            alt=""
            layout="fill"
            src="https://images.unsplash.com/photo-1587293852726-70cdb56c2866?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3272&q=80"
          />
          {children}
          <FooterSmall absolute />
        </section>
      </main>
    </>
  )
}

export default Auth
