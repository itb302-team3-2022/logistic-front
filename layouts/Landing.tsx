import { Navbar, Footer } from 'components'

type Props = {
  children: React.ReactNode
}

const Landing: React.FC<Props> = ({ children }) => {
  return (
    <>
      <Navbar transparent />
      <main>{children}</main>
      <Footer />
    </>
  )
}

export default Landing
