import 'styles/globals.scss'
import type { AppProps } from 'next/app'
import { Provider } from 'react-redux'
import { store } from 'store'
import { NextPage } from 'next/types'
import { ReactElement, ReactNode } from 'react'
import { AnimatePresence } from 'framer-motion'
import { Alert } from '@/components'

export type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode
}

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout
}

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? ((page) => page)
  return getLayout(
    <Provider store={store}>
      <Component {...pageProps} />
      <AnimatePresence>
        <Alert />
      </AnimatePresence>
    </Provider>
  )
}

export default MyApp
