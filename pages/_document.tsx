import { Head, Html, Main, NextScript } from 'next/document'

const MyDocument = () => {
  return (
    <Html>
      <Head />
      <body data-theme="lofi">
        <Main />
        <div id="portal" />
        <NextScript />
      </body>
    </Html>
  )
}

export default MyDocument
