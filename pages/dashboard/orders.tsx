import { Pill } from '@/components'
import {
  AcceptRejectModal,
  UpdateOrderModal,
  ViewOrderDetailModal,
} from '@/components/Containers'
import Portal from '@/HOC/Portal'
import { Dispatch, RootState, store } from '@/store'
import { EnumOrderStatus, Order } from '@/utils/common/type'
import {
  getDate,
  getShipmentStatusStyle,
  getShortID,
  getStatusStyle,
  getTime,
  stringIncludes,
} from '@/utils/helper'
import { AnimatePresence } from 'framer-motion'
import { Dashboard } from 'layouts'
import { ReactElement, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { NextPageWithLayout } from '../_app'

enum EnumModal {
  AcceptRejectModal = 'accept-reject-modal',
  ViewShipment = 'view-shipment',
  UpdateModal = 'update-modal',
}

type ModalStates = {
  [key in EnumModal]: {
    order: Order
    meta: any
  } | null
}

const OrderPage: NextPageWithLayout = () => {
  const orders = useSelector((state: RootState) => state.logistic.orders)
  const dispatch = useDispatch<Dispatch>()
  const initModalStates: ModalStates = {
    [EnumModal.AcceptRejectModal]: null,
    [EnumModal.ViewShipment]: null,
    [EnumModal.UpdateModal]: null,
  }
  const [filteredItems, setFilteredItems] = useState<Order[]>(orders)
  const [search, setSearch] = useState<string>('')
  const [modalStates, setModalStates] = useState<ModalStates>(initModalStates)

  useEffect(() => {
    store.dispatch.logistic.fetchAllOrders()
  }, [])

  const onClick = (modal: EnumModal, id?: string | null, meta?: string) => {
    if (id) {
      const order = orders.find((o) => o._id === id) as Order
      setModalStates((prev) => ({
        ...prev,
        [modal]: {
          order,
          meta,
        },
      }))
      return
    }
    setModalStates(initModalStates)
  }
  const onButtonClick = (e: any, id: string) => {
    e.stopPropagation()
    switch (e.target.id) {
      case 'accept':
        onClick(EnumModal.AcceptRejectModal, id, 'accept')
        break
      case 'reject':
        onClick(EnumModal.AcceptRejectModal, id, 'reject')
        break
      case 'update':
        onClick(EnumModal.UpdateModal, id)
        break
      default:
        onClick(EnumModal.ViewShipment, id)
        break
    }
  }

  useEffect(() => {
    if (search) {
      const _filteredItems = orders.filter((item, index) => {
        const {
          _id,
          trackId,
          customer,
          createdAt,
          statusType,
          shipmentStatusType,
        } = item
        const customerId = _id.substring(16)
        if (
          stringIncludes(customerId, search) ||
          stringIncludes(trackId, search) ||
          stringIncludes(customer.name, search) ||
          stringIncludes(customer.accountNumber, search) ||
          stringIncludes(statusType, search) ||
          stringIncludes(shipmentStatusType, search)
        ) {
          return item
        }
      })
      setFilteredItems(_filteredItems)
      return
    }

    setFilteredItems(orders)
  }, [search, orders])

  return (
    <div>
      <input
        type="text"
        placeholder="Search (ID / Account No / Company Name / Email / Address )"
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        className="input input-sm w-full mb-4"
      />

      <div className="overflow-x-auto w-full">
        <table className="table hover w-full">
          <thead>
            <tr>
              <th>Order ID</th>
              <th>Track ID</th>
              <th>Customer</th>
              <th>Received At</th>
              <th>Status</th>
              <th>Shipment Status</th>
              <th>Action</th>
              {/* <th>Charge</th> */}
            </tr>
          </thead>

          <tbody>
            {filteredItems.map(
              (
                {
                  _id,
                  trackId,
                  customer,
                  createdAt,
                  statusType,
                  shipmentStatusType,
                },
                i
              ) => {
                return (
                  <tr
                    key={_id}
                    className="hover cursor-pointer"
                    onClick={(e) => onButtonClick(e, _id)}
                  >
                    <td className="text-sm">{getShortID(_id)}</td>
                    <td>{trackId}</td>
                    <td>
                      <div className="flex items-left flex-col">
                        <div className="font-bold">{customer.name}</div>
                        <div className="text-sm opacity-50">
                          {customer.accountNumber}
                        </div>
                      </div>
                    </td>
                    <td>
                      <div>{getDate(createdAt)}</div>
                      <div className="text-sm opacity-50">
                        {getTime(createdAt)}
                      </div>
                    </td>
                    <td>
                      <Pill
                        className={getStatusStyle(statusType)}
                        text={statusType.toUpperCase()}
                      />
                    </td>
                    <td>
                      <Pill
                        className={getShipmentStatusStyle(shipmentStatusType)}
                        text={shipmentStatusType.toUpperCase()}
                      />
                    </td>
                    <td>
                      {statusType === EnumOrderStatus.New ? (
                        <div className="btn-group flex-nowrap">
                          <button
                            id="accept"
                            className="btn btn-accent btn-sm text-white"
                            onClick={(e) => onButtonClick(e, _id)}
                          >
                            Accept
                          </button>
                          <button
                            className="btn btn-error hover:bg-red-500 btn-sm text-white"
                            id="reject"
                            onClick={(e) => onButtonClick(e, _id)}
                          >
                            Reject
                          </button>
                        </div>
                      ) : (
                        <button
                          className="btn btn-sm"
                          disabled={statusType === EnumOrderStatus.Completed}
                          // onClick={() => onClick(EnumModal.UpdateModal, _id)}
                          onClick={(e) => onButtonClick(e, _id)}
                          id="update"
                        >
                          update
                        </button>
                      )}
                    </td>
                  </tr>
                )
              }
            )}
          </tbody>
        </table>
      </div>
      <Portal>
        <AnimatePresence>
          {modalStates['accept-reject-modal'] && (
            <AcceptRejectModal
              order={modalStates['accept-reject-modal'].order}
              meta={modalStates['accept-reject-modal'].meta}
              onClose={() => onClick(EnumModal.AcceptRejectModal)}
            />
          )}
          {modalStates['update-modal'] && (
            <UpdateOrderModal
              order={modalStates['update-modal'].order}
              onClose={() => onClick(EnumModal.UpdateModal)}
            />
          )}
          {modalStates['view-shipment'] && (
            <ViewOrderDetailModal
              order={modalStates['view-shipment'].order}
              onClose={() => onClick(EnumModal.UpdateModal)}
            />
          )}
        </AnimatePresence>
      </Portal>
    </div>
  )
}

export default OrderPage

OrderPage.getLayout = (page: ReactElement) => <Dashboard>{page}</Dashboard>
