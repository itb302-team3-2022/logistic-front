import { Modal } from '@/components'
import Portal from '@/HOC/Portal'
import { Dispatch, RootState } from '@/store'
import { Customer, Product } from '@/utils/common/type'
import { stringIncludes } from '@/utils/helper'
import { Dashboard } from 'layouts'
import { ReactElement, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

const ProductsPage = () => {
  const customers = useSelector((state: RootState) => state.logistic.customers)
  const dispatch = useDispatch<Dispatch>()

  const [filteredItems, setFilteredItems] = useState<Customer[]>(customers)
  const [search, setSearch] = useState<string>('')

  useEffect(() => {
    dispatch.logistic.fetchAllCustomers()
  }, [])

  useEffect(() => {
    if (search) {
      const _search = search.toLowerCase()
      const _filteredItems = customers.filter((item, index) => {
        const { _id, email, accountNumber, name, address } = item
        const customerId = _id.substring(16)
        if (
          stringIncludes(customerId, search) ||
          stringIncludes(name, search) ||
          stringIncludes(address, search) ||
          stringIncludes(email, search) ||
          stringIncludes(customerId, search) ||
          stringIncludes(accountNumber, search)
        ) {
          return item
        }
      })
      setFilteredItems(_filteredItems)
      return
    }

    setFilteredItems(customers)
  }, [search, customers])

  return (
    <div>
      <input
        type="text"
        placeholder="Search (ID / Account No / Company Name / Email / Address )"
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        className="input input-sm w-full mb-4"
      />

      <div className="overflow-x-auto w-full">
        <table className="table hover w-full">
          <thead>
            <tr>
              <th>ID</th>
              <th>Account No</th>
              <th>Company Name</th>
              <th>Email</th>
              <th>API key</th>
              <th>Bills</th>
              <th>Orders</th>
              <th>action</th>
            </tr>
          </thead>

          <tbody>
            {filteredItems.map(
              (
                { _id, name, address, email, accountNumber, bills, orders, apiKey },
                i
              ) => {
                return (
                  <tr key={_id}>
                    <td className="text-sm">{_id.substring(16)}</td>
                    <td>{accountNumber}</td>
                    <td>
                      <div className="flex items-left flex-col">
                        <div className="font-bold">{name}</div>
                        <div className="text-sm opacity-50">{address}</div>
                      </div>
                    </td>
                    <td>{email}</td>
                    <td>{apiKey}</td>
                    <td>{bills.length}</td>
                    <td>{orders.length}</td>
                    <td>
                      <button className="btn btn-error btn-sm">Suspend</button>
                    </td>
                  </tr>
                )
              }
            )}
          </tbody>
          {/* <tfoot>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Price</th>
              <th>Orders</th>
            </tr>
          </tfoot> */}
        </table>
      </div>
    </div>
  )
}

export default ProductsPage

ProductsPage.getLayout = (page: ReactElement) => <Dashboard>{page}</Dashboard>
