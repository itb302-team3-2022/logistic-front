import { Landing } from 'layouts'
import { ReactElement, useState } from 'react'
import { orderBy, uniqBy } from 'lodash'
import API from '@/api'
import { useDispatch } from 'react-redux'
import { Dispatch } from '@/store'
import { EnumMessageType, ShipmentStatus } from '@/utils/common/type'
import dayjs from 'dayjs'

const HomePage = () => {
  const [input, setInput] = useState<string>('')
  const dispatch = useDispatch<Dispatch>()
  const [results, setResults] = useState<ShipmentStatus[]>([])

  const onSubmit = async () => {
    if (!input) return

    const _trackIds = input.replace(/\s+/g, '')

    // remove duplicate ids
    const trackIds = (uniqBy(_trackIds.split(','), (e) => e)).join()

    try {
      const _resp = await API.fetchShipmentStatus(trackIds)

      const resp = _resp.map((r) => {
        const shipmentTimeline = orderBy(r.shipmentTimeline, ['time'], ['desc'])
        return { ...r, shipmentTimeline }
      })

      setResults(resp)
    } catch (err: any) {
      dispatch.app.setAlertMessageAsync({
        type: EnumMessageType.Fail,
        text: 'Something wrong',
      })
    }
  }

  const showResults = results.length > 0

  return (
    <div className="relative pt-16 pb-32 flex content-center items-center justify-center h-screen">
      <div
        className="absolute top-0 w-full h-full bg-center bg-cover"
        style={{
          backgroundImage:
            "url('https://images.unsplash.com/photo-1536599018102-9f803c140fc1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2456&q=80')",
        }}
      >
        <span
          id="blackOverlay"
          className="w-full h-full absolute opacity-75 bg-black"
        ></span>
      </div>
      <div className="container relative mx-auto">
        <div className="items-center flex flex-wrap">
          <div className="w-full lg:w-6/12 px-4 ml-auto mr-auto text-center">
            <div className="mb-4">
              <h1 className="text-white font-semibold text-5xl mb-5">
                Track ID
              </h1>
              <div className="flex space-x-3">
                <input
                  className="input flex-grow"
                  value={input}
                  onChange={(e) => {
                    setResults([])
                    setInput(e.target.value)
                  }}
                  placeholder="Enter a trackID"
                />
                <button className="btn btn-warning" onClick={onSubmit}>
                  Submit
                </button>
              </div>
            </div>

            {showResults && (
              <div className="bg-white p-4 px-4 rounded-lg max-h-80 overflow-auto">
                <ul className=''>
                  {results?.map((result, i) => {
                    return (
                      <li key={i} className="flex flex-col items-start mb-4">
                        <code className="mb-4">{result.trackId}</code>
                        <div className="text-sm w-full relative space-y-3 timeline-status">
                          {result.shipmentTimeline.map((shipment, i) => {
                            return (
                              <div key={i} className="grid grid-cols-3 place-items-start gap-y-5">
                                <div className='z-10'>
                                  <span
                                    className={`font-bold rounded-full px-3 py-1 text-xs ${
                                      i === 0 ? 'bg-warning ' : 'bg-gray-400 font-normal text-white'
                                    }`}
                                  >
                                    {shipment.eventStatus.toUpperCase()}
                                  </span>
                                </div>
                                <div className="col-span-2 text-left">
                                  <p>
                                    {dayjs(shipment.time).format(
                                      'DD-MM-YYYY HH:mm A'
                                    )}
                                  </p>
                                  <p>{shipment.title}</p>
                                  <p className='text-xs text-gray-500'>{shipment.message}</p>
                                </div>
                              </div>
                            )
                          })}
                        </div>
                        <hr />
                      </li>
                    )
                  })}
                </ul>
              </div>
            )}
          </div>
        </div>
      </div>
      <div
        className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden h-16"
        style={{ transform: 'translateZ(0)' }}
      >
        <svg
          className="absolute bottom-0 overflow-hidden"
          xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="none"
          version="1.1"
          viewBox="0 0 2560 100"
          x="0"
          y="0"
        >
          <polygon
            className="text-white fill-current"
            points="2560 0 2560 100 0 100"
          ></polygon>
        </svg>
      </div>
    </div>
  )
}

export default HomePage

HomePage.getLayout = (page: ReactElement) => <Landing>{page}</Landing>
