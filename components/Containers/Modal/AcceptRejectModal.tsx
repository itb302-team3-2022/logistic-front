import { Pill, Modal } from '@/components'
import { Dispatch } from '@/store'
import {
  EnumModalAnimate,
  EnumOrderStatus,
  EnumShipmentStatus,
  Order,
  UpdateShipmentData,
} from '@/utils/common/type'
import { DEFAULT_ACCEPT_REASON, DEFAULT_REJECTED_REASON } from '@/utils/const'
import { getShipmentStatusStyle, getStatusStyle } from '@/utils/helper'
import { ChangeEvent, ChangeEventHandler, useState } from 'react'
import { useDispatch } from 'react-redux'
import EventInputs from './EventInputs'

type Props = {
  order: Order
  onClose: (e?: any) => void
  meta: string
}

const UpdateOrderModal: React.FC<Props> = ({ order, onClose, meta }) => {
  const isAccepted = meta === 'accept'
  const [input, setInput] = useState<typeof DEFAULT_ACCEPT_REASON>(
    isAccepted ? DEFAULT_ACCEPT_REASON : DEFAULT_REJECTED_REASON
  )
  const orderStatus = isAccepted
    ? EnumOrderStatus.Processing
    : EnumOrderStatus.Rejected
  const shipmentStatus = isAccepted
    ? EnumShipmentStatus.Preparing
    : EnumShipmentStatus.Pending

  const dispatch = useDispatch<Dispatch>()

  const onSubmit = async () => {
    if (!input.title || !input.message) return

    const update = {
      data: {
        status: orderStatus,
        // shipmentStatus,
        event: { ...input, eventStatus: shipmentStatus },
      },
      trackId: order.trackId,
    }

    try {
      await dispatch.logistic.addShipmentTimeline(update)
      onClose()
    } catch {}
  }

  const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setInput((prev) => ({ ...prev, [e.target.id]: e.target.value }))
  }

  // useEffect(() => {
  //   if (error) {
  //     setTimeout(() => {
  //       setError('')
  //     }, 3000)
  //   }
  // }, [error])

  return (
    <Modal
      backdrop
      title={`${meta.toLocaleUpperCase()} order`}
      animateStyle={EnumModalAnimate.FromBottom}
      onClose={onClose}
    >
      <div className="flex flex-col justify-center p-4 space-y-5">
        <div className="flex justify-between">
          <div>This Order Status will change to</div>
          <Pill
            className={`${getStatusStyle(orderStatus)}`}
            text={orderStatus.toUpperCase()}
          />
        </div>
        <div className="flex justify-between">
          <div>This Shipment Status will change to</div>
          <Pill
            className={`${getShipmentStatusStyle(shipmentStatus)}`}
            text={shipmentStatus.toUpperCase()}
          />
        </div>
        <hr />
        <EventInputs input={input} onInputChange={onInputChange} />
        <hr />
        {/* bottom */}
        <div className="flex space-x-2 items-stretch justify-items-stretch">
          <button
            className="btn flex-grow bg-slate-300 text-black border-0 hover:text-black hover:bg-slate-400"
            onClick={onClose}
          >
            Cancel
          </button>
          <button className="btn flex-grow" onClick={onSubmit}>
            Submit
          </button>
        </div>
      </div>
    </Modal>
  )
}

export default UpdateOrderModal
