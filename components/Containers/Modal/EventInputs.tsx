import { DEFAULT_ACCEPT_REASON } from '@/utils/const'
import React from 'react'

type Props = {
  input: typeof DEFAULT_ACCEPT_REASON
  onInputChange: (e?: any) => void
}

const EventInputs: React.FC<Props> = ({ input, onInputChange }) => {
  return (
    <div className="w-96 flex flex-col space-y-5">
      <div className="flex flex-col">
        <span className="text-sm text-gray-600 mb-1">Event Title</span>
        <input
          value={input.title}
          id="title"
          placeholder="Title"
          className="input input-bordered mb-4"
          onChange={(e) => onInputChange(e)}
        />
        <span className="text-sm text-gray-600 mb-1">Event Description</span>
        <input
          id="message"
          value={input.message}
          placeholder="Message"
          className="input input-bordered"
          onChange={(e) => onInputChange(e)}
        />
      </div>
    </div>
  )
}

export default EventInputs
