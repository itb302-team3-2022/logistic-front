import { Pill, Modal } from '@/components'
import { Dispatch } from '@/store'
import {
  EnumMessageType,
  EnumModalAnimate,
  EnumOrderStatus,
  EnumShipmentStatus,
  Order,
} from '@/utils/common/type'
import { DEFAULT_ACCEPT_REASON } from '@/utils/const'
import { getDate, getShortID, getStatusStyle, getTime } from '@/utils/helper'
import { faArrowRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import dayjs from 'dayjs'
import { capitalize, orderBy } from 'lodash'
import { ChangeEvent, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import styles from './Common.module.scss'

type Props = {
  order: Order
  onClose: (e?: any) => void
  // meta: string
}

const orderStatusMapper = (status: EnumShipmentStatus): EnumOrderStatus => {
  switch (status) {
    case EnumShipmentStatus.Pending:
      return EnumOrderStatus.Paused
    case EnumShipmentStatus.Preparing:
    case EnumShipmentStatus.Picking:
    case EnumShipmentStatus.Shipping:
      return EnumOrderStatus.Processing
    case EnumShipmentStatus.Shipped:
      return EnumOrderStatus.Completed
  }
}

const ViewOrderDetailModal: React.FC<Props> = ({ order, onClose }) => {
  const sortedTimeline = orderBy(order.shipmentTimeline, ['time'], ['desc'])

  const [showChargeInput, setShowChargeInput] = useState<boolean>(false)
  const [charge, setCharge] = useState<number>(order.charge)
  const [chargeTemp, setChargeTemp] = useState<number>(order.charge)
  const dispatch = useDispatch<Dispatch>()

  const onUpdateCharge = async () => {
    // if value dosen't change, just toggle back the input
    if (charge === order.charge) {
      setShowChargeInput(false)
      return
    } 

    try {
      await dispatch.logistic.updateShipmentCharge({
        data: { charge },
        trackId: order.trackId,
      })
      setChargeTemp(charge)
      setShowChargeInput(false)
    } catch {}
  }

  const renderChargeInput = () => {
    return showChargeInput ? (
      <div className="ml-4 my-3 flex flex-col">
        <input
          className="input input-bordered flex-grow input-sm mb-2"
          type="number"
          value={charge.toString()}
          onChange={(e) => setCharge(+e.target.value)}
        />
        <button className="btn btn-xs" onClick={onUpdateCharge}>
          Update
        </button>
      </div>
    ) : (
      <button
        className="btn btn-warning btn-xs"
        onClick={() => setShowChargeInput(true)}
      >
        Set Charge
      </button>
    )
  }

  return (
    <Modal
      backdrop
      title="Order Detail"
      animateStyle={EnumModalAnimate.FromBottom}
      onClose={onClose}
    >
      <div
        className="w-full sm:w-96 p-5 flex flex-col space-y-5"
        style={{ width: '80vw', height: '80vh' }}
      >
        <div>
          <h2 className={styles.title}>Order Detail</h2>
          <div className={styles.split}>
            {/* left */}
            <div className={styles.col}>
              <div className={styles.row}>
                <div className={styles.rowTitle}>Track ID</div>
                <div className={styles.rowValue}>{order.trackId}</div>
              </div>
              <div className={styles.row}>
                <div className={styles.rowTitle}>Order ID</div>
                <div className={styles.rowValue}>{getShortID(order._id)}</div>
              </div>
              <div className={styles.row}>
                <div className={styles.rowTitle}>Order Date</div>
                <div className={styles.rowValue}>
                  {getDate(order.createdAt)} {getTime(order.createdAt)}
                </div>
              </div>
              <div className={styles.row}>
                <div className={styles.rowTitle}>Customer Name</div>
                <div className={styles.rowValue}>
                  {order.customer.name} ({order.customer.accountNumber})
                </div>
              </div>
              <div className={styles.row}>
                <div className={`${styles.rowTitle}`}>Pickup from</div>
                <div className={`${styles.rowValue} text-gray-700`}>
                  <div>
                    <p>{order.shipFrom.name}</p>
                    <p>{order.shipFrom.phone}</p>
                    <p>{order.shipFrom.phone2}</p>
                    <p>{order.shipFrom.address}</p>
                  </div>
                </div>
              </div>
              <div className={styles.row}>
                <div className={`${styles.rowTitle}`}>Ship to</div>
                <div className={`${styles.rowValue} text-gray-700`}>
                  <div>
                    <p>{order.shipTo.name}</p>
                    <p>{order.shipTo.phone}</p>
                    <p>{order.shipTo.phone2}</p>
                    <p>{order.shipTo.address}</p>
                  </div>
                </div>
              </div>
              <div className={styles.row}>
                <div className={`${styles.rowTitle}`}>
                  Parcels Pickup Period
                </div>
                <div className={`${styles.rowValue} text-gray-700`}>
                  <div className="flex justify-between  w-4/5">
                    <span>From</span>
                    <code>{getDate(order.pickUp.startTime.toString())}</code>
                  </div>
                  <div className="flex justify-between  w-4/5">
                    <span>Until</span>
                    <code>{getDate(order.pickUp.endTime.toString())}</code>
                  </div>
                </div>
              </div>

              <div className={styles.row}>
                <div className={`${styles.rowTitle}`}>Parcels</div>
                <div className={`${styles.rowValue} text-gray-700`}>
                  <ul>
                    {order.parcels.map((p, i) => (
                      <li
                        key={i}
                        className="flex justify-between items-center w-4/5"
                      >
                        <code>{p.refId}</code>
                        {/* <button className="btn btn-warning btn-xs">
                          measurement
                        </button> */}
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
              <div className={styles.row}>
                <div className={`${styles.rowTitle}`}>Shipment Charge</div>
                <div className={`${styles.rowValue} text-gray-700`}>
                  <div className="flex justify-between items-center w-4/5">
                    <span>${chargeTemp}</span>
                    {renderChargeInput()}
                  </div>
                </div>
              </div>
            </div>
            {/* right */}
            <div className={styles.col}>
              <div className={styles.row}>
                <div className={`${styles.rowTitle} ${styles.single}`}>
                  Shipment Timeline
                </div>
              </div>
              <div className={styles.row}>
                <div className={`${styles.rowValue} ${styles.single} relative`}>
                  <ul className={styles.timelineContainer}>
                    {sortedTimeline.map((s, i) => {
                      const isHighlight = i === 0
                      const isPending = s.eventStatus === 'pending'
                      const highlightStyle = isHighlight
                        ? isPending
                          ? 'text-error'
                          : 'text-primary'
                        : ''
                      return (
                        <li key={i} className="flex mt-4">
                          <div className={`w-20`}>
                            <span
                              className={`${styles.statusPill} ${
                                isHighlight ? styles.hightlight : ''
                              } ${isPending ? styles.pending : ''}`}
                            >
                              {s.eventStatus.toUpperCase()}
                            </span>
                          </div>
                          <div className="flex flex-col">
                            <p className={`${highlightStyle} text-sm`}>
                              {dayjs(s.time).format('DD-MM-YYYY HH:mm A')}
                            </p>
                            <p className={`${highlightStyle}`}>{s.title}</p>
                            <p className={styles.statusDesc}>{s.message}</p>
                          </div>
                        </li>
                      )
                    })}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

        <hr />
      </div>
    </Modal>
  )
}

export default ViewOrderDetailModal
