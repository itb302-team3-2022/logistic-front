import Modal from '@/components/Modal'
import Pill from '@/components/Pill'
import { Dispatch } from '@/store'
import {
  EnumMessageType,
  EnumModalAnimate,
  EnumOrderStatus,
  EnumShipmentStatus,
  Order,
} from '@/utils/common/type'
import { DEFAULT_ACCEPT_REASON } from '@/utils/const'
import { getStatusStyle } from '@/utils/helper'
import { faArrowRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { capitalize } from 'lodash'
import { ChangeEvent, ChangeEventHandler, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import EventInputs from './EventInputs'

type Props = {
  order: Order
  onClose: (e?: any) => void
  // meta: string
}

const orderStatusMapper = (status: EnumShipmentStatus): EnumOrderStatus => {
  switch (status) {
    case EnumShipmentStatus.Pending:
      return EnumOrderStatus.Paused
    case EnumShipmentStatus.Preparing:
    case EnumShipmentStatus.Picking:
    case EnumShipmentStatus.Shipping:
      return EnumOrderStatus.Processing
    case EnumShipmentStatus.Shipped:
      return EnumOrderStatus.Completed
  }
}

const UpdateOrderModal: React.FC<Props> = ({ order, onClose }) => {
  const [input, setInput] = useState<typeof DEFAULT_ACCEPT_REASON>({
    title: '',
    message: '',
  })

  const [orderStatus, setOrderStatus] = useState<EnumOrderStatus>(
    order.statusType
  )
  const [shipmentStatus, setShipmentStatus] = useState<EnumShipmentStatus>(
    order.shipmentStatusType
  )

  const onSelectChange = (e: any) => {
    setShipmentStatus(e.target.value)
  }

  const isOrderStatusUpdate = orderStatus !== order.statusType
  const dispatch = useDispatch<Dispatch>()

  const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setInput((prev) => ({ ...prev, [e.target.id]: e.target.value }))
  }

  const onSubmit = async () => {
    if (!input.title || !input.message) {
      dispatch.app.setAlertMessageAsync({
        type: EnumMessageType.Warning,
        text: 'Please input title and message'
      })
      
      return 
    }

    const update = {
      data: {
        status: orderStatus,
        // shipmentStatus,
        event: { ...input, eventStatus: shipmentStatus },
      },
      trackId: order.trackId,
    }

    try {
      await dispatch.logistic.addShipmentTimeline(update)
      onClose()
    } catch {}
  }

  useEffect(() => {
    setOrderStatus(orderStatusMapper(shipmentStatus))
  }, [shipmentStatus])

  return (
    <Modal
      backdrop
      title="Update Shipment"
      animateStyle={EnumModalAnimate.FromBottom}
      onClose={onClose}
    >
      <div className="flex flex-col justify-center p-4 space-y-5">
        <div className="flex justify-between items-center">
          <div className="flex-grow font-bold">Order Status</div>
          {/* current */}
          <Pill
            className={`${getStatusStyle(order.statusType)}`}
            text={order.statusType.toUpperCase()}
          />
          {isOrderStatusUpdate && (
            <>
              <FontAwesomeIcon
                className="text-gray-400 px-2"
                icon={faArrowRight}
              />
              <Pill
                className={`${getStatusStyle(orderStatus)}`}
                text={orderStatus.toUpperCase()}
              />
            </>
          )}
        </div>
        <div className="flex justify-between items-center">
          <div className="flex-grow font-bold">Shipment Status</div>
          <select
            className="select select-bordered w-40"
            onChange={onSelectChange}
            value={shipmentStatus}
          >
            {Object.values(EnumShipmentStatus).map((status, i) => {
              return (
                <option key={i} value={status}>
                  {capitalize(status)}
                  {status === order.shipmentStatusType ? ' (Current)' : ''}
                </option>
              )
            })}
          </select>
        </div>
        <hr />
        <EventInputs input={input} onInputChange={onInputChange} />
        <hr />
        {/* bottom */}
        <div className="flex space-x-2 items-stretch justify-items-stretch">
          <button
            className="btn flex-grow bg-slate-300 text-black border-0 hover:text-black hover:bg-slate-400"
            onClick={onClose}
          >
            Cancel
          </button>
          <button className="btn flex-grow" onClick={onSubmit}>
            Submit
          </button>
        </div>
      </div>
    </Modal>
  )
}

export default UpdateOrderModal
