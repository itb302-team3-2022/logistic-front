import Link from 'next/link'
import React from 'react'

type props = {
  absolute?: boolean
}

const Small: React.FC<props> = ({ absolute = false }) => {
  return (
    <>
      <footer
        className={
          (absolute ? 'absolute w-full bottom-0 bg-primary' : 'relative') +
          ' pb-6'
        }
      >
        <div className="container mx-auto px-4">
          <hr className="mb-6 border-b-1 border-blueGray-600" />
          <div className="flex flex-wrap items-center justify-center">
            <div className="w-full md:w-4/12 px-4">
              <div className="text-sm text-white font-semibold py-1 text-center">
                Copyright © {new Date().getFullYear()}{' '}
                <Link href="/">
                  <a
                    className="text-white hover:text-secondary text-sm font-semibold py-1"
                  >
                    SL Express Limited
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  )
}

export default Small
