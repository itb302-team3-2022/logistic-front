import { motion } from 'framer-motion'
import React, { CSSProperties } from 'react'
import { faClose } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styles from './Modal.module.scss'
import { getModalAnimationConfig } from 'utils/helper'
import { EnumModalAnimate } from '@/utils/common/type'

type Props = {
  title?: string
  onClose?: (e: any) => void
  position?: 'center' | 'bottom'
  modalStyle?: CSSProperties
  animateStyle?: EnumModalAnimate
  backdrop?: boolean
}

const Modal: React.FC<Props> = ({
  children,
  position = 'center',
  onClose,
  title,
  modalStyle,
  animateStyle = EnumModalAnimate.FromRight,
  backdrop = true,
}) => {
  const config = getModalAnimationConfig(animateStyle)
  return (
    <div className={styles.wrapper}>
      {backdrop && <div className={styles.backdrop} onClick={onClose} />}
      <motion.div
        initial={config.initial}
        animate={config.animate}
        exit={config.exit}
        className={`${styles.container} ${
          position === 'bottom' ? styles.bottom : styles.center
        }`}
        style={modalStyle}
      >
        {title && (
          <div className={styles.top}>
            <span className={styles.title}>{title}</span>
            {onClose && (
              <button className={styles.closeBtn} onClick={onClose}>
                <FontAwesomeIcon icon={faClose} />
              </button>
            )}
          </div>
        )}
        <div className="overflow-auto">{children}</div>
      </motion.div>
    </div>
  )
}

export default Modal
