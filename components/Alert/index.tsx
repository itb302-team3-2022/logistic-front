/* eslint-disable react/no-unknown-property */
import Portal from '@/HOC/Portal'
import { RootState } from '@/store'
import { EnumMessageType } from '@/utils/common/type'
import {
  faCheck,
  faClose,
  faWarning,
  IconDefinition,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { AnimatePresence, motion } from 'framer-motion'
import { useSelector } from 'react-redux'

const getAlertIcon = (type: EnumMessageType): IconDefinition => {
  switch (type) {
    case EnumMessageType.Fail:
      return faClose
    case EnumMessageType.Warning:
      return faWarning
    case EnumMessageType.Success:
      return faCheck
  }
}
const getAlertColor = (type: EnumMessageType): string => {
  switch (type) {
    case EnumMessageType.Fail:
      return 'bg-red-400 text-white'
    case EnumMessageType.Warning:
      return 'bg-orange-500 text-white'
    case EnumMessageType.Success:
      return 'bg-green-500 text-white'
  }
}

const Alert = () => {
  const alertMessage = useSelector((state: RootState) => state.app.alertMessage)

  const hidden = {
    y: -100,
    opacity: 0,
  }
  const show = {
    y: 0,
    opacity: 1,
  }

  return (
    <Portal>
      {alertMessage && (
        <motion.div
          initial={hidden}
          animate={show}
          exit={hidden}
          className="absolute w-full top-4 z-50 flex justify-center items-center"
        >
          <div
            className={`space-x-0 space-y-0 w-full mx-4 sm:w-96 alert alert-warning text-bold text drop-shadow-2xl flex flex-row items-center ${getAlertColor(
              alertMessage.type
            )}`}
          >
            <FontAwesomeIcon icon={getAlertIcon(alertMessage.type)} />
            <span className="flex-grow">{alertMessage.text}</span>
          </div>
        </motion.div>
      )}
    </Portal>
  )
}

export default Alert
