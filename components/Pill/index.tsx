import React from 'react'

type Props = {
  text: string
  className: string
}

const Pill: React.FC<Props> = ({ text, className }) => {
  return (
    <div className={`inline rounded-full px-2 py-1 mx-1 text-xs ${className}`}>
      {text}
    </div>
  )
}

export default Pill
