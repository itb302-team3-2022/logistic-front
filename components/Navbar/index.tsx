import React from 'react'
import Link from 'next/link'
import Image from 'next/image'

// import PagesDropdown from "components/Dropdowns/PagesDropdown.js";

type Props = {
  transparent?: boolean
}

const Navbar: React.FC<Props> = () => {
  const [navbarOpen, setNavbarOpen] = React.useState(false)
  return (
    <nav className="top-0 fixed z-50 w-full flex flex-wrap items-center justify-between px-2 py-3 navbar-expand-lg">
      <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
        <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
          <Link href="/">
            <div className='w-32 h-10 mt-5'>
                <Image
                  src="/assets/logo_white@2x.png"
                  layout="responsive"
                  objectFit='cover'
                  width={279}
                  height={123}
                  alt="logo"
                />
            </div>
          </Link>
          <button
            className="cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none"
            type="button"
            onClick={() => setNavbarOpen(!navbarOpen)}
          >
            <i className="text-white fas fa-bars"></i>
          </button>
        </div>
        <div
          className={
            'lg:flex flex-grow items-center bg-white lg:bg-opacity-0 lg:shadow-none' +
            (navbarOpen ? ' block rounded shadow-lg' : ' hidden')
          }
          id="example-navbar-warning"
        >
          <ul className="flex flex-col lg:flex-row list-none lg:ml-auto">
            <li className="flex items-center">{/* <PagesDropdown /> */}</li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Navbar
