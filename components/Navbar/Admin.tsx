import React from 'react'
import { useRouter } from 'next/router'

// import UserDropdown from "components/Dropdowns/UserDropdown.js";

const Navbar = () => {
  const router = useRouter()

  const title = router.pathname.split('/').pop()

  return (
    <div className="relative w-full">
      {/* Navbar */}
      <nav className="absolute top-0 left-0 w-full z-10 bg-transparent md:flex-row md:flex-nowrap md:justify-start flex items-center p-4">
        <div className="w-full mx-auto items-center flex justify-between md:flex-nowrap flex-wrap md:px-10 px-4">
          {/* Brand */}
          <a
            className="text-3xl capitalize hidden lg:inline-block font-semibold mt-3 -ml-8"
            href="#"
            onClick={(e) => e.preventDefault()}
          >
            {title}
          </a>
          {/* User */}
          <ul className="flex-col md:flex-row list-none items-center hidden md:flex"></ul>
        </div>
      </nav>
      {/* End Navbar */}
    </div>
  )
}

export default Navbar
