import { createModel } from '@rematch/core'
import { AlertMessage, AppState } from 'utils/common/type'
import type { RootModel } from '.'

export const app = createModel<RootModel>()({
  state: {
    alertMessage: null,
  } as AppState,
  reducers: {
    setAlertMessage(state, alertMessage: AlertMessage) {
      return { ...state, alertMessage }
    },
    clearAlertMessage(state) {
      return { ...state, alertMessage: null }
    },
  },
  effects: (dispatch) => ({
    setAlertMessageAsync(message: AlertMessage) {
      dispatch.app.setAlertMessage(message)
      setTimeout(() => {
        dispatch.app.clearAlertMessage()
      }, 3000)
    },
  }),
  //   selectors: (slice, createSelector, hasProps) => ({
  //   })
})
