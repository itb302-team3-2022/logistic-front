import { Models } from '@rematch/core'
import { app } from './app'
import { logistic } from './logistic'

export interface RootModel extends Models<RootModel> {
  app: typeof app
  logistic: typeof logistic
}

export const models: RootModel = { app, logistic }
