import { SetShipmentCharge } from '@/utils/common/api/Request/types'
import { createModel } from '@rematch/core'
import API from 'api'
import {
  Customer,
  EnumMessageType,
  LogisticState,
  Order,
  UpdateShipmentChargeData,
  UpdateShipmentData,
} from 'utils/common/type'
import type { RootModel } from '.'

export const logistic = createModel<RootModel>()({
  state: {
    orders: [],
    customers: [],
  } as LogisticState,
  reducers: {
    setOrders(state, orders: Order[]) {
      return { ...state, orders }
    },
    setCustomers(state, customers: Customer[]) {
      return { ...state, customers }
    },
  },
  effects: (dispatch) => ({
    async fetchAllOrders() {
      try {
        const orders = await API.fetchAllOrders()
        dispatch.logistic.setOrders(orders)
      } catch (err) {
        dispatch.app.setAlertMessageAsync({
          type: EnumMessageType.Fail,
          text: 'Unable to fetch orders',
        })
      }
    },
    async fetchAllCustomers() {
      try {
        const customers = await API.fetchAllCustomers()
        dispatch.logistic.setCustomers(customers)
      } catch (err) {
        dispatch.app.setAlertMessageAsync({
          type: EnumMessageType.Fail,
          text: 'Unable to fetch customers',
        })
      }
    },

    async addShipmentTimeline({
      data,
      trackId,
    }: UpdateShipmentData): Promise<void> {
      return new Promise(async (resolve, reject) => {
        try {
          await API.addShipmentTimeline(data, trackId)
          dispatch.logistic.fetchAllOrders()
          dispatch.app.setAlertMessageAsync({
            type: EnumMessageType.Success,
            text: 'Update successfully',
          })
          resolve()
        } catch (err) {
          reject(err)
          dispatch.app.setAlertMessageAsync({
            type: EnumMessageType.Fail,
            text: 'Unable to update the shipment status',
          })
        }
      })
    },
    async updateShipmentCharge({
      data,
      trackId,
    }: UpdateShipmentChargeData): Promise<void> {
      return new Promise(async (resolve, reject) => {
        try {
          await API.updateShipmentCharge(data, trackId)
          dispatch.app.setAlertMessageAsync({
            type: EnumMessageType.Success,
            text: 'Update successfully',
          })
          dispatch.logistic.fetchAllOrders()
          resolve()
        } catch (err) {
          reject(err)
          dispatch.app.setAlertMessageAsync({
            type: EnumMessageType.Fail,
            text: 'Unable to update the shipment charge',
          })
        }
      })
    },
  }),
})
