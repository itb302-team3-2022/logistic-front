import { SetShipmentCharge, SetShipmentStatus } from '@/utils/common/api/Request/types'
import { ResponseShipmentState } from '@/utils/common/api/Response/type'
import { Methods } from 'utils/common/api'
import request from './request'

const API = {
  fetchShipmentStatus(trackIds: string): Promise<ResponseShipmentState[]> {
    return request(Methods.Get, `/shipment?tid=${trackIds}`)
  },
  fetchAllOrders() {
    return request(Methods.Get, `/orders`)
  },
  fetchAllCustomers() {
    return request(Methods.Get, `/customers`)
  },
  addShipmentTimeline(data: SetShipmentStatus, trackId: string) {
    return request(Methods.Put, `/orders/${trackId}`, data)
  },
  updateShipmentCharge(data: SetShipmentCharge, trackId: string) {
    return request(Methods.Put, `/orders/charge/${trackId}`, data)
  }
}

export default API
