import axios, { Method } from 'axios'
import { Request, Methods } from 'utils/common/api'
import { stringify } from 'qs'

const base = process.env.API_SERVER || 'http://192.168.1.15:4000' + '/api'

const request = async (
  method: Method,
  endpoint: string,
  data: Request.Data | null = null,
  options: Request.Options = {}
) => {
  const { cancelToken = null, authorize = true } = options
  const getData = method === Methods.Get ? data : null
  const postData = method === Methods.Post || Methods.Put? data : null
  const url = `${base}${endpoint}`
  const req: Request.Config = {
    url,
    method,
    paramsSerializer: (params) => {
      // convert normal array to string array with quote 👉 "[1,2,3]"
      Object.keys(params).forEach((p) => {
        if (Array.isArray(params[p])) {
          params[p] = `[${params[p]}]`
        }
      })
      return stringify(params)
    },
  }

  if (getData) {
    req.params = getData
  }

  if (postData) {
    req.data = postData
  }

  if (cancelToken) {
    req.cancelToken = cancelToken
  }

  const { data: json } = await axios(req)
  return json
}

export default request
