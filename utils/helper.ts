import dayjs from 'dayjs'
import {
  MakeCustomValueType,
  TargetAndTransition
} from 'framer-motion/types/types'
import {
  EnumModalAnimate,
  EnumOrderStatus,
  EnumShipmentStatus
} from './common/type'

export const getDate = (timestamp: string) => {
  return dayjs(timestamp).format('DD-MM-YYYY')
}
export const getTime = (timestamp: string) => {
  return dayjs(timestamp).format('HH:mm:ss')
}

type StatusStyle = {
  [key in EnumOrderStatus]: string
}

type ShipmentStatusStyle = {
  [key in EnumShipmentStatus]: string
}

export const getStatusStyle = (status: EnumOrderStatus): string => {
  const statusStyleMap: StatusStyle = {
    [EnumOrderStatus.Completed]: 'text-green-500 border-2 border-green-500',
    [EnumOrderStatus.New]: 'text-red-500 border-2 border-red-500',
    [EnumOrderStatus.Processing]: 'text-blue-500 border-2 border-blue-500',
    [EnumOrderStatus.Paused]: 'text-orange-500 border-2 border-orange-500',
    [EnumOrderStatus.Rejected]: 'text-purple-500 border-2 border-purple-500',
    // [EnumOrderStatus.Shipping]: 'text-teal-500 border-2 border-teal-500',
    // [EnumOrderStatus.Shipped]:
    //   'bg-teal-500 text-white border-2 border-teal-500',
  }

  return statusStyleMap[status]
}
export const getShipmentStatusStyle = (status: EnumShipmentStatus): string => {
  const statusStyleMap: ShipmentStatusStyle = {
    [EnumShipmentStatus.Pending]:
      'bg-red-500 text-white border-2 border-red-500',
    [EnumShipmentStatus.Picking]:
      'bg-purple-500 text-white border-2 border-purple-500',
    [EnumShipmentStatus.Preparing]:
      'bg-lime-500 text-white border-2 border-lime-500',
    [EnumShipmentStatus.Shipped]:
      'bg-blue-500 text-white border-2 border-blue-500',
    [EnumShipmentStatus.Shipping]:
      'bg-green-500 text-white border-2 border-green-500',
    // [EnumOrderStatus.Shipping]: 'text-teal-500 border-2 border-teal-500',
    // [EnumOrderStatus.Shipped]:
    //   'bg-teal-500 text-white border-2 border-teal-500',
  }

  return statusStyleMap[status]
}

export const getLowerCase = (str: string) => {
  return str.toLowerCase()
}

export const stringIncludes = (original: string, matcher: string) => {
  return getLowerCase(original).includes(getLowerCase(matcher))
}

export const getShortID = (str: string) => {
  return str.substring(16)
}

// export const getCustomerFullName = (customer: Customer) => {
//   return `${customer.firstName} ${customer.lastName}`
// }

export type FramerAnimateConfig = {
  initial: MakeCustomValueType<any>
  animate: TargetAndTransition
  exit: TargetAndTransition
}

export const getModalAnimationConfig = (
  animateStyle: EnumModalAnimate
): FramerAnimateConfig => {
  const modalAnimation: {
    [key in EnumModalAnimate]: FramerAnimateConfig
  } = {
    [EnumModalAnimate.FromBottom]: {
      initial: {
        opacity: 0,
        y: 300,
      },
      animate: {
        opacity: 1,
        y: 0,
      },
      exit: {
        opacity: 0,
        y: 300,
      },
    },
    [EnumModalAnimate.FromTop]: {
      initial: {
        opacity: 0,
        y: -100,
      },
      animate: {
        opacity: 1,
        y: 0,
      },
      exit: {
        opacity: 0,
        y: -100,
      },
    },
    [EnumModalAnimate.FromLeft]: {
      initial: {
        opacity: 0,
        x: -100,
      },
      animate: {
        opacity: 1,
        x: 0,
      },
      exit: {
        opacity: 0,
        x: -100,
      },
    },
    [EnumModalAnimate.FromRight]: {
      initial: {
        opacity: 0,
        x: 100,
      },
      animate: {
        opacity: 1,
        x: 0,
      },
      exit: {
        opacity: 0,
        x: 100,
      },
    },
    [EnumModalAnimate.ZoomIn]: {
      initial: {
        opacity: 0,
        scale: 1.2,
      },
      animate: {
        opacity: 1,
        scale: 1,
      },
      exit: {
        opacity: 0,
        scale: 2,
      },
    },
    [EnumModalAnimate.ZoomOut]: {
      initial: {
        opacity: 0,
        scale: 0.8,
      },
      animate: {
        opacity: 1,
        scale: 1,
      },
      exit: {
        opacity: 0,
        scale: 0,
      },
    },
  }
  return modalAnimation[animateStyle]
}
