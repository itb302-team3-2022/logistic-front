import { EnumOrderStatus, EnumShipmentStatus } from '../../type'

export type SetShipmentStatus = {
  status: EnumOrderStatus
  // shipmentStatus: EnumShipmentStatus,
  event: {
    title: string
    message: string
    eventStatus: EnumShipmentStatus
  }
}
export type SetShipmentCharge = {
  charge: number
}
