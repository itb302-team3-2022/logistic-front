export * as Request from './Request'

export const enum Methods {
  Post = 'post',
  Get = 'get',
  Put = 'put',
}
