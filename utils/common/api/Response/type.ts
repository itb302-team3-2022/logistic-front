import { Parcels, PickUp, ShipFrom, ShipmentTimeline, ShipTo } from '../../type'

export type ResponseShipmentState = {
  customer: {
    accountNumber: string
    companyName: string
  }
  parcels: Parcels[]
  pickUp: PickUp
  shipFrom: ShipFrom
  shipTo: ShipTo
  shipmentStatusType: any
  shipmentTimeline: ShipmentTimeline[]
  trackId: string
}
