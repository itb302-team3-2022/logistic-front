import { SetShipmentCharge, SetShipmentStatus } from "./api/Request/types"

/**
 * All Enum
 *
 */
export enum EnumWeightUnit {
  Kg = 'kg',
  Lb = 'lb',
  G = 'g',
}

export enum EnumDimensionUnit {
  Cm = 'cm',
  Ft = 'ft',
  Inch = 'inch',
  M = 'm',
}

export enum EnumCurrency {
  Hkd = 'hkd',
  Usd = 'usd',
  Rmb = 'rmb',
}


export enum EnumModalAnimate {
  FromBottom = 'bottom',
  FromTop = 'top',
  FromLeft = 'left',
  FromRight = 'right',
  ZoomIn = 'zoomin',
  ZoomOut = 'zoomout',
}


export enum EnumMessageType {
  Success,
  Fail,
  Warning,
}

export enum EnumShipmentStatus {
  Pending = 'pending',
  Preparing = 'preparing',
  Picking = 'picking',
  Shipping = 'shipping',
  Shipped = 'shipped',
}

export enum EnumOrderStatus {
  New = 'new',
  Paused = 'paused',
  Rejected = 'rejected',
  Processing = 'processing',
  Completed = 'completed'
}

export type ShipFrom = {
  name: string
  phone: string
  phone2?: string
  address: string
}

export type ShipTo = {
  name: string
  phone: string
  phone2?: string
  address: string
}

export type Parcel = {
  refId: string
  weight?: {
    value: number
    unit: EnumWeightUnit
  }
  dimension?: {
    depth: number
    width: number
    height: number
    unit: EnumDimensionUnit
  }
  description: string
  insuredValue?: {
    amount: number
    currency: EnumCurrency
  }
}

export type AlertMessage = {
  type: EnumMessageType
  text: string
}

export type PickUp = {
  startTime: number // timestamp,
  endTime: number
}

export type Delivery = {
  receiver: string
  phone: string
  phone2: string
  address: string
  _id: string
}



export type Customer = {
  id: string,
  _id: string,
  name: string,
  address: string,
  email: string
  orders: []
  bills: []
  apiKey: string,
  createdAt: string
  updatedAt: string
  accountNumber: string,
  // _id: string
  // firstName: string
  // lastName: string
  // email: string
  // orders: Order[]
  // delivery: Delivery
}

export type OrderItems = {
  productId: string
  qty: number
  price: number
  _id: string
}

export type ShipmentTimeline = {
  time: string
  title: string
  message: string
  eventStatus: EnumShipmentStatus
  _id: string
}

export type Product = {
  _id: string
  name: string
  images: string[]
  description: string
  price: string
  createdAt: string
  updatedAt: string
}



export type Order = {
  shipFrom: ShipFrom,
  shipTo: ShipTo,
  pickUp: PickUp,
  _id: string,
  parcels: Parcel[]
  statusType: EnumOrderStatus,
  shipmentTimeline: ShipmentTimeline[],
  customer: Customer
  charge: number,
  trackId: string,
  shipmentStatusType: EnumShipmentStatus,
  createdAt: string
  updatedAt: string
}

export type ShipmentStatus = {
  customer: {
    accountNumber: string
    companyName: string
  }
  parcels: Parcel[]
  pickUp: PickUp
  shipFrom: ShipFrom
  shipTo: ShipTo
  shipmentStatusType: any
  shipmentTimeline: ShipmentTimeline[]
  trackId: string
}

export type UpdateShipmentData = {
  data: SetShipmentStatus,
  trackId: string
}

export type UpdateShipmentChargeData = {
  data: SetShipmentCharge,
  trackId: string
}



/**
 * Redux state
 */

export type AppState = {
  alertMessage: AlertMessage | null
}
export type LogisticState = {
  customers: Customer[],
  orders: Order[]
}
