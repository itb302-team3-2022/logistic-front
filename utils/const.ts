export const DEFAULT_ACCEPT_REASON={
    message: "We will handle your order shortly",
    title: "Shipment accepted"
}

export const DEFAULT_REJECTED_REASON={
    message: "Please settled the bill",
    title: "Shipment Rejected"
}